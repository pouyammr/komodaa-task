import { useState } from 'react';

const useSyncState = (initialValue?: any) => {
  const [value, setValue] = useState(initialValue);
  let latestValue = value ?? null;

  const get = () => latestValue;

  const set = (newValue: unknown) => {
    if (Array.isArray(newValue))
      latestValue = [...newValue];
    else if (typeof newValue == 'object' && newValue !== null)
      latestValue = {...newValue};
    else if (typeof newValue == 'number' || typeof newValue == 'boolean' || typeof newValue == 'string' || typeof newValue == 'symbol' || typeof newValue == 'bigint') {
      latestValue = newValue;
    }
    else
      return;
    setValue(latestValue);
  }

  return [get(), set]
}

export default useSyncState;