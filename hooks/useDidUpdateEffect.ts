import React, { useEffect, useRef } from "react";

const useDidUpdateEffect = (fn: React.EffectCallback, deps?: React.DependencyList) => {
  const didMountRef = useRef<boolean>(false);
  useEffect(() => {
    if (didMountRef.current)
      return fn()
    else
      didMountRef.current = true
  }, deps)
}

export default useDidUpdateEffect;