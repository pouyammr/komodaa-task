import React from "react";
import { Platform, StyleSheet, View } from 'react-native';
import { Avatar, Colors, IconButton } from "react-native-paper";

const MainScreenHeader = ({ userAvatarUrl }) => {
  return (
    <View style={[Platform.OS === 'web' ? styles['web-container'] : styles['mobile-container'], styles['container']]}>
      <View style={[Platform.OS === 'web' ? styles['web-avatar'] : null, styles['avatar']]}>
        <Avatar.Image
        size={50} 
        source={{uri: userAvatarUrl ?? ""}} />
      </View>
      <View style={[Platform.OS === 'web' ? styles['web-search'] : null, styles['search']]}>
        <IconButton icon="search-outline" size={30} color={Colors.black} />
      </View>
      <View style={[Platform.OS === 'web' ? styles['web-menu'] : null, styles['menu']]}>
        <IconButton icon="menu-outline" size={38} style={styles['menu-icon']} color={Colors.black} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    width: '100%',
    marginTop: Platform.OS !== 'web' ? 54 : 0,
    paddingHorizontal: 24,
  },
  'web-container': {
    position: 'relative'
  },
  'mobile-container': {
    flexDirection: 'row-reverse'
  },
  'avatar': {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#E6EEFE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  'web-avatar': {
    position: 'absolute',
    top: 24,
    right: 24
  },
  'search': {
    marginRight: 19,
  },
  'web-search': {
    position: 'absolute',
    top: 26,
    right: 103
  },
  'menu': {
    margin: 0,
    flexGrow: 1,
  },
  'web-menu': {
    position: 'absolute',
    top: 24,
    left: 32
  },
  'menu-icon': {
    marginTop: 6,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
})

export default MainScreenHeader;
