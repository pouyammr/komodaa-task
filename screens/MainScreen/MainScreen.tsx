import React, { useEffect } from 'react';

import { View, StyleSheet, Platform } from 'react-native';
import MainScreenHeader from './MainScreenHeader';
import TopDesigners from '../../components/TopDesigners';
import PopularDesigns from '../../components/PopularPosts';

import allActions from '../../store/actions/allActions';
import { useDispatch, useSelector } from 'react-redux';

const MainScreen: React.FC = () => {
  const user = useSelector(state => state.user);
  const topDesigners = useSelector(state => state.topDesigners);
  const posts = useSelector(state => state.posts);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allActions.getDesigners());
    dispatch(allActions.getUser('Phillip'));
    dispatch(allActions.getPosts());
  }, []);
  

  return (
    <View style={styles['container']}>
      <MainScreenHeader userAvatarUrl={user.avatarUrl} />
      <TopDesigners users={topDesigners} style={styles['designers']} />
      <PopularDesigns posts={posts} style={styles['posts']} />
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FFF'
  },
  'designers': {
    marginTop: Platform.OS === 'web' ? 140 : 40
  },
  'posts' : {
    marginTop: 36
  }
})

export default MainScreen;