import React from 'react';
import { View, Image, StyleSheet, Dimensions, ScrollView } from 'react-native';
import BottomSheetHeader from '../../components/BottomSheetHeader';

import { useNavigation, useRoute } from '@react-navigation/core';

import { useSelector } from 'react-redux';

import { Headline, Text } from 'react-native-paper';

import { PostScreenNavigationProps, PostScreenRouteProps } from '../../navigation/MainNavigator';
import PostScreenHeader from './PostScreenHeader';
import { FlatList } from 'react-native-gesture-handler';
import { ClientPost } from '../../store/types/postTypes';



const PostScreen: React.FC = () => {
  const route = useRoute<PostScreenRouteProps>();
  const navigation = useNavigation<PostScreenNavigationProps>();
  const post = useSelector(state => state.posts.find(post => post.postId === route.params.postId));
  const posts = useSelector(state => state.posts.filter(post => post.postId !== route.params.postId));

  const renderItem = ({ item }: { item: ClientPost }) => {
    return (
      <Image source={{uri: item.postImageUrl}} style={styles['gallery-image']} />
    )
  }

  return (
    <View style={styles['container']}>
      <Image source={{uri: post.postImageUrl}} style={styles['post-image']} />
      <PostScreenHeader navigation={navigation} post={post} />

      <ScrollView showsVerticalScrollIndicator={false} style={styles['bottom-sheet-container']}>
        <BottomSheetHeader post={post} />
        <View style={styles['post']}>
          <Text style={styles['post-text']}>{post.caption}</Text>
        </View>
        <View style={styles['gallery']}>
          <Headline style={styles['gallery-header']}>Gallery</Headline>
          <View style={styles['gallery-posts']}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={posts}
              keyExtractor={(item) => item.postId}
              renderItem={renderItem} />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    flex: 1,
    flexDirection: 'column',
    position: 'relative'
  },
  'post-image': {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: Dimensions.get('screen').height
  },
  'bottom-sheet-container': {
    width: '100%',
    height: Dimensions.get('screen').height / 3,
    backgroundColor: '#F4F6FC',
    elevation: 3,
    shadowColor: '#00000029',
    shadowOffset: {width: 0, height: 3},
    shadowRadius: 6,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    zIndex: 5,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  },
  'post': {
    width: '100%',
    paddingHorizontal: 41,
    marginTop: 23
  },
  'post-text': {
    fontFamily: 'sen',
    fontSize: 14,
    lineHeight: 21,
    textAlign: 'left'
  },
  'gallery': {
    width: '100%',
    paddingHorizontal: 41,
    marginTop: 22,
  },
  'gallery-header': {
    fontFamily: 'sen',
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'left'
  },
  'gallery-posts': {
    width: '100%',
    marginTop: 19,
    paddingBottom: 30
  },
  'gallery-image': {
    width: 130,
    height: 198,
    marginRight: 12,
    borderRadius: 8
  }
})

export default PostScreen;