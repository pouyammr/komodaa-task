import React from "react";
import { View, StyleSheet, Platform, StyleProp, ViewStyle } from 'react-native';
import { IconButton } from "react-native-paper";

import { useDispatch } from "react-redux";

import { PostScreenNavigationProps } from "../../navigation/MainNavigator";
import allActions from "../../store/actions/allActions";
import { ClientPost } from "../../store/types/postTypes";

interface PostScreenHeaderProps {
  navigation: PostScreenNavigationProps,
  post: ClientPost,
  style?: StyleProp<ViewStyle>
}

const PostScreenHeader: React.FC<PostScreenHeaderProps> = ({ navigation, post, style }) => {
  const dispatch = useDispatch();
  
  const handleClick = () => {
    post.liked
    ? dispatch(allActions.unlikePost(post.postId))
    : dispatch(allActions.likePost(post.postId))
  }

  return (
    <View style={[Platform.OS === 'web' ? styles['web-container'] : styles['mobile-container'], style, styles['container']]}>
      <IconButton 
        icon="share-social-outline" 
        size={20}
        style={[Platform.OS === 'web' ? styles['web-share']: null, styles['share']]} />
      <IconButton 
        icon={!post.liked ? "heart-outline" : "heart"}
        size={20}
        onPress={() => handleClick()} 
        style={[Platform.OS === 'web' ? styles['web-like'] : null, styles['like']]} />
      <IconButton
        icon="arrow-back-outline"
        size={20}
        onPress={() => navigation.goBack()}
        style={[Platform.OS === 'web' ? styles['web-back'] : null, styles['back']]} />
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    width: '100%',
    zIndex: 5,
    marginTop: Platform.OS !== 'web' ? 54 : 0,
    paddingHorizontal: 32,
  },
  'web-container': {
    position: 'relative'
  },
  'mobile-container': {
    flexDirection: 'row-reverse'
  },
  'share': {
    zIndex: 5
  },
  'web-share': {
    position: 'absolute',
    top: 26,
    right: 32
  },
  'like': {
    zIndex: 5,
    marginRight: 24
  },
  'web-like': {
    zIndex: 5,
    position: 'absolute',
    top: 26,
    right: 76
  },
  'back': {
    zIndex: 5,
    flexGrow: 1,
    alignItems: 'flex-start'
  },
  'web-back': {
    position: 'absolute',
    top: 26
  }
})

export default PostScreenHeader;