import React, { useState } from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';

import store from './store/store';
import { Provider as StoreProvider } from 'react-redux';

import { Provider as PaperProvider, DefaultTheme, configureFonts } from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationContainer } from '@react-navigation/native';
import { Stack } from './navigation/MainNavigator';

import MainScreen from './screens/MainScreen/MainScreen';
import PostScreen from './screens/PostScreen/PostScreen';

const fetchFonts = () => {
  return Font.loadAsync({
    'sen' : require('./assets/fonts/Sen-Regular.ttf'),
    'sen-bold': require('./assets/fonts/Sen-Bold.ttf')
  })
};

export default function App() {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  if (!isLoaded)
    return <AppLoading startAsync={fetchFonts} onError={err => console.log(err)} onFinish={() => setIsLoaded(true)} />

  const fontConfig = {
    web: {
      regular: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      bold: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      medium: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      light: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      thin: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
    },
    android: {
      regular: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      bold: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      medium: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      light: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      thin: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
    },
    ios: {
      regular: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      bold: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      medium: {
        fontFamily: 'sen-bold',
        fontWeight: 'normal'
      },
      light: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
      thin: {
        fontFamily: 'sen',
        fontWeight: 'normal'
      },
    }
  } as const;

  const theme = {
    ...DefaultTheme,
    font: configureFonts(fontConfig)
  }

  return (
    <StoreProvider store={store}>
      <PaperProvider theme={theme} settings={{icon: props => <Ionicons {...props} />}}>
            <NavigationContainer>
              <Stack.Navigator initialRouteName="Home" screenOptions={{headerShown: false}}>
                <Stack.Screen name="Home" component={MainScreen} />
                <Stack.Screen name="Post" component={PostScreen} />
              </Stack.Navigator>
            </NavigationContainer>
      </PaperProvider>
    </StoreProvider>
  );
}
