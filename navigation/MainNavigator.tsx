import { RouteProp } from "@react-navigation/native";
import { createNativeStackNavigator, NativeStackNavigationProp } from "@react-navigation/native-stack";

type RootStackParamList = {
  Home: undefined,
  Post: {postId: string}
}

export type HomeScreenNavigationProps = NativeStackNavigationProp<RootStackParamList, 'Home'>
export type PostScreenNavigationProps = NativeStackNavigationProp<RootStackParamList, 'Post'>

export type PostScreenRouteProps = RouteProp<RootStackParamList, 'Post'>

export const Stack = createNativeStackNavigator<RootStackParamList>();