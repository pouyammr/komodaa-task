import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { IconButton, Title } from "react-native-paper";
import MasonryList from "@react-native-seoul/masonry-list";
import PostCard from "./PostCard";

import { ClientPost, ClientPosts } from "../../store/types/postTypes";
import { StyleProp, ViewStyle } from 'react-native';

interface PopularDesignProps {
  posts: ClientPosts,
  children?: React.ReactNode,
  style?: StyleProp<ViewStyle>
}

const PopularDesigns: React.FC<PopularDesignProps> = ({ posts, style }) => {
  const renderItem = ({ item }: { item: ClientPost, index?: number }) => {
    return (
      <PostCard key={item.postId} style={{margin: 7, borderRadius: 8}} item={item} />
    )
  }

  return (
    <View style={[style, styles['container']]}>
      <View style={styles['header']}>
        <Title style={styles['header-text']}>Popular Designs</Title>
        <IconButton icon="arrow-forward-outline" size={30} />
      </View>
      <View style={{width: '100%', height: '100%'}}>
      <MasonryList 
        numColumns={2} 
        data={posts} 
        renderItem={renderItem} 
        contentContainerStyle={styles['list-container']} 
        showsVerticalScrollIndicator={false} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    width: '100%',
    paddingBottom: 28,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  'header': {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
  },
  'header-text': {
    fontFamily: 'sen-bold',
    fontSize: 24,
    lineHeight: 28,
    textAlign: 'left',
  },
  'list-container': {
    paddingHorizontal: 25,
    paddingBottom: 350
  }
});

export default PopularDesigns;