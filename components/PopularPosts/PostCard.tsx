import React, { useMemo } from "react";
import { Image, StyleSheet } from 'react-native';
import { TouchableRipple } from "react-native-paper";

import { ClientPost } from "../../store/types/postTypes";
import { StyleProp, ViewStyle } from 'react-native';
import { PostScreenNavigationProps } from "../../navigation/MainNavigator";

import { useNavigation } from "@react-navigation/core";

const PostCard: React.FC<{item: ClientPost, style?: StyleProp<ViewStyle>}> = ({ style, item }) => {
  const navigation = useNavigation<PostScreenNavigationProps>();
  const randomNum = useMemo(() => Math.random(), []);
  return (  
  <TouchableRipple 
    onPress={() => navigation.navigate('Post', {postId: item.postId})} 
    style={[style, {...styles['card'], height: randomNum < 1/3 ? 117 : (randomNum >= 1/3 && randomNum < 2/3) ? 168 : randomNum >= 2/3 ? 191 : 217 }]}>
      <Image source={{uri: item.postImageUrl}} style={styles['img']} />
  </TouchableRipple>)
}

const styles = StyleSheet.create({
  'card': {
    width: '95%',
    borderRadius: 4,
  },
  'img': {
    width: '100%', 
    height: '100%',
    borderRadius: 8,
  }
})

export default PostCard;