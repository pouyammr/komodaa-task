import React, { useCallback, useEffect, useState } from "react";
import useSyncState from "../../hooks/useSyncState";

import { View, ScrollView, StyleSheet, StyleProp, ViewStyle } from "react-native";
import { Platform } from "react-native";

import { Avatar, Caption, IconButton, Title } from "react-native-paper";

import { User, Users } from "../../store/types/userTypes";

interface TopDesignersProps {
  users: Users,
  style?: StyleProp<ViewStyle>
}

const TopDesigners: React.FC<TopDesignersProps> = ({ users, style }) => {
  const [sortType, setSortType] = useState<"ascending" | "descending">("ascending");
  const [sortedUsers, setSortedUsers] = useSyncState(users);
  const sortUsers = useCallback((usrs: Users, type: "ascending" | "descending") => {
    return usrs.sort((a, b) => {
      if (type == "ascending") {
        if (a.name > b.name)
          return 1
        if (a.name < b.name)
          return -1
        return 0 
      }
      else {
        if (a.name < b.name)
          return 1
        if (a.name > b.name)
          return -1
        return 0 
      }
    })
  }, [sortType, users])

  useEffect(() => setSortedUsers(sortUsers(users, sortType)), [sortUsers])

  return (
    <View style={[style, styles['container']]}>
      <View style={styles['header']}>
        <Title selectable={false} style={styles['header-title']}>Top Designer</Title>
        <IconButton 
        icon={sortType == "ascending" ? "arrow-forward-outline" : "arrow-back-outline"} 
        size={30} 
        onPress={() => setSortType(prevState => prevState == "ascending" ? "descending" : "ascending")} />
      </View >
      <View style={styles['avatars']}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {sortedUsers.map((el: User) => {
            return (
              <View key={el.userId} style={styles['avatar-container']}>
                <Avatar.Image source={{uri: el.avatarUrl}} size={70} />
                <Caption selectable={false} style={styles['caption']}>{el.name}</Caption>
              </View>
            )
          })}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    width: '100%',
    height: 132,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  'header': {
    width: '100%',
    height: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
  },
  'header-title': {
    fontSize: 26,
    lineHeight: 28,
    fontFamily: 'sen-bold',
    textAlign: "left"
  },
  'avatars': {
    marginTop: Platform.OS == 'android' ? 20 : 30,
    paddingHorizontal: 32
  },
  'avatar-container': {
    width: 70,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    marginRight: 20
  },
  'caption': {
    marginTop: 17,
    fontSize: 12,
    textAlign: 'center'
  }
})

export default TopDesigners;