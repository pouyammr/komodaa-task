import React from "react";
import { View, StyleSheet } from 'react-native';
import { Avatar, Title, Caption, Headline, Text } from "react-native-paper";

import { ClientPost } from "../../store/types/postTypes";

interface BottomSheetHeaderProps {
  post: ClientPost
}

const BottomSheetHeader: React.FC<BottomSheetHeaderProps> = ({ post }) => {
  return (
    <View style={styles['container']}>
      <View style={styles['headline']}>
        <Headline style={styles['headline-text']}>{post.title}</Headline>
      </View>
      <View style={styles['info']}>
        <View style={styles['avatar']}>
          <Avatar.Image source={{uri: post.author.avatarUrl}} size={40} />
        </View>
        <View style={styles['info-text']}>
          <Title style={styles['title']}>{post.author.name}</Title>
          <Caption style={styles['caption']}>{post.author.position}</Caption>
        </View>
      </View>
      <View style={styles['tag-container']}>
        {post.tags.map(tag => {
          const superScriptRegex = /\^/g;
          let tagText: string | string[];
          if (superScriptRegex.test(tag))
            tagText = tag.split("^")
          else
            tagText = tag;
          return (
            <View style={styles['tag']} key={tag}>
              {Array.isArray(tagText)
              ? <View style={{flexDirection: 'row'}}>
              <Text style={styles['tag-text']}>{tagText[0]}</Text>
              <Text style={styles['sup']}>{tagText[1]}</Text>
              </View>
              : <Text style={styles['tag-text']}>{tagText}</Text>}
            </View>
          )
        })}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  'container': {
    width: '100%',
    paddingHorizontal: 41,
    paddingTop: 22,
    flexDirection: 'column',
  },
  'headline': {
    width: '100%',
    marginBottom: 21
  },
  'headline-text': {
    fontFamily: 'sen',
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'left',
  },
  'info': {
    width: '100%',
    flexDirection: 'row',
    marginBottom: 18
  },
  'avatar': {
    width: 36,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
    marginRight: 14
  },
  'info-text': {
    flex: 1,
    flexDirection: 'column'
  },
  'title': {
    fontFamily: 'sen-bold',
    fontSize: 15,
    lineHeight: 29,
    textAlign: 'left',
    marginBottom: 4
  },
  'caption': {
    fontFamily: 'sen',
    fontSize: 10,
    lineHeight: 12,
    textAlign: 'left'
  },
  'tag-container': {
    width: '100%',
    flexDirection: 'row'
  },
  'tag': {
    paddingHorizontal: 14,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 14,
    borderRadius: 4,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#DEDCDC'
  },
  'tag-text': {
    fontFamily: 'sen',
    fontSize: 12,
    lineHeight: 14,
    textAlign: 'left'
  },
  'sup': {
  fontFamily: 'sen',
  fontSize: 7,
  lineHeight: 7,
  textAlign: 'left'
  }
})

export default BottomSheetHeader;