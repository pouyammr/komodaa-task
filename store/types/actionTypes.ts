const actionTypes = {
  setDesigners: "SET_DESIGNERS",
  setPosts: "SET_POSTS",
  setUser: "SET_USER",
} as const;

export default actionTypes;