import { User } from "./userTypes";

export type ClientPost = {
  liked: boolean,
  postId: string,
  title: string,
  author: User,
  tags: string[],
  caption: string,
  postImageUrl: string
};

export type ClientPosts = ClientPost[];

export type ServerPost = Omit<ClientPost, 'author'> & { author: string };

export type ServerPosts = ServerPost[];