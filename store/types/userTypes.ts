export type User = {
  position: string,
  userId: string,
  name: string,
  avatarUrl: string
}

export type Users = User[];
