import fetcher from '../../api/fetcher';
import { AxiosResponse } from 'axios';

import actionTypes from "../types/actionTypes";
import { Dispatch } from 'redux'

import { ClientPost, ClientPosts, ServerPosts } from "../types/postTypes";
import { Users } from '../types/userTypes';

import { Alert } from 'react-native';

export const setPosts = (payload: ClientPosts | []) => {
  return {
    type: actionTypes.setPosts,
    payload: payload
  }
}

export const getPosts = () => {
  return async (dispatch: Dispatch) => {
    try {
      const posts: AxiosResponse<ServerPosts> = await fetcher.get('/posts.json');
      try {
        const users: AxiosResponse<Users> = await fetcher.get('/users.json');
        const userKeys = users.status === 200 ? Object.keys(users.data) : null;
        const postKeys = posts.status === 200 ? Object.keys(posts.data) : null;

        const data: ClientPosts | [] = postKeys.map(postKey => {
            const post: ClientPost = {
              ...posts.data[postKey],
              author: users.data[userKeys.find(userKey => users.data[userKey].name === posts.data[postKey].author)] 
            }
            return post;
          });
        dispatch(setPosts(data))
      } catch(err) {
        Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
      }
    } catch(err) {
      Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
    }
  }
}

export const likePost = (postId: string) => {
  return async (dispatch: Dispatch) => {
    try {
      const posts: AxiosResponse<ServerPosts> = await fetcher.get('/posts.json');
      const postKeys = Object.keys(posts.data);
      const updatedPosts = {...posts.data};
      updatedPosts[postKeys.find(key => posts.data[key].postId === postId)].liked = true;
      try {
        await fetcher.put('/posts.json', updatedPosts);
        try {
          const users: AxiosResponse<Users> = await fetcher.get('/users.json');
          const userKeys = users.status === 200 ? Object.keys(users.data) : null;
  
          const data: ClientPosts | [] = postKeys.map(postKey => {
              const post: ClientPost = {
                ...updatedPosts[postKey],
                author: users.data[userKeys.find(userKey => users.data[userKey].name === updatedPosts[postKey].author)] 
              }
              return post;
            });
          dispatch(setPosts(data))
        } catch(err) {
          Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
        }
      } catch(err) {
        Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
      }
    } catch(err) {
      Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
    }
  }
}

export const unlikePost = (postId: string) => {
  return async (dispatch: Dispatch) => {
    try {
      const posts: AxiosResponse<ServerPosts> = await fetcher.get('/posts.json');
      const postKeys = Object.keys(posts.data);
      const updatedPosts = {...posts.data};
      updatedPosts[postKeys.find(key => posts.data[key].postId === postId)].liked = false;
      try {
        await fetcher.put('/posts.json', updatedPosts);
        try {
          const users: AxiosResponse<Users> = await fetcher.get('/users.json');
          const userKeys = users.status === 200 ? Object.keys(users.data) : null;
  
          const data: ClientPosts | [] = postKeys.map(postKey => {
              const post: ClientPost = {
                ...updatedPosts[postKey],
                author: users.data[userKeys.find(userKey => users.data[userKey].name === updatedPosts[postKey].author)] 
              }
              return post;
            });
          dispatch(setPosts(data))
        } catch(err) {
          Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
        }
      } catch(err) {
        Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
      }
    } catch(err) {
      Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
    }
  }
}
