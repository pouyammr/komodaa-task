import { setUser, getUser } from "./user";
import { setDesigners, getDesigners } from "./designers";
import { setPosts, getPosts, likePost, unlikePost } from "./posts";

const allActions = {
  setUser,
  getUser,
  setDesigners,
  getDesigners,
  setPosts,
  getPosts,
  likePost,
  unlikePost
}

export default allActions;