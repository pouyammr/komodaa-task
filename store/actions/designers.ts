import fetcher from '../../api/fetcher';
import { AxiosResponse } from 'axios';

import actionTypes from "../types/actionTypes";
import { Dispatch } from 'redux';

import { User, Users } from "../types/userTypes";

import { Alert } from 'react-native';

export const setDesigners = (payload: (User | {})[]) => {
  return {
    type: actionTypes.setDesigners,
    payload: payload
  }
}

export const getDesigners = () => {
  return async (dispatch: Dispatch) => {
    try {
      const namesRes: AxiosResponse<string[]> = await fetcher.get('/topDesigners.json');
      try {
        const users: AxiosResponse<Users> = await fetcher.get('/users.json');
        const userKeys = Object.keys(users.data);
        const topDesigners = namesRes.data.map(el => {
          const user: User | {} = users.status === 200 ? users.data[userKeys.find(key => users.data[key].name === el)] : {};
           return user
        })
        dispatch(setDesigners(topDesigners))
      } catch(err) {
        Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
      }
    } catch(err) {
      Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
    }
  }
}
