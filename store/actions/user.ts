import fetcher from '../../api/fetcher';
import { AxiosResponse } from 'axios';

import actionTypes from "../types/actionTypes";
import { Dispatch } from 'redux';

import { User, Users } from "../types/userTypes";

import { Alert } from 'react-native';

export const setUser = (payload: User | {}) => {
  return {
    type: actionTypes.setUser,
    payload: payload
  }
}

export const getUser = (userName: string) => {
  return async (dispatch: Dispatch) => {
    try {
      const res: AxiosResponse<Users> = await fetcher.get('/users.json');
      const keys = Object.keys(res.data);
      const data: User | {} = res.status === 200 ? res.data[keys.find(el => res.data[el].name === userName)] : {};
      dispatch(setUser(data))
    } catch(err) {
      Alert.alert("Error!", err, [{text: "Ok", style: 'cancel'}]);
    }
  }
}
