import actionTypes from "../types/actionTypes";
import { AnyAction } from 'redux';
import { DefaultRootState } from "react-redux";

const initialState = {
  user: {
    name: "",
    avatarUrl: "",
    userId: ""
  },
  topDesigners: [],
  posts: [], 
}

const rootReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.setUser: {
      return {
        ...state,
        user: action.payload
      }
    }
    case actionTypes.setDesigners: {
      return {
        ...state,
        topDesigners: action.payload
      }
    }
    case actionTypes.setPosts: {
      return {
        ...state,
        posts: action.payload
      }
    }
    default: {
      return {...state}
    }
  }
}

export default rootReducer;