import axios from 'axios';

const fetcher = axios.create({
  baseURL: "https://komodaa-task-default-rtdb.firebaseio.com",
  headers: {
    "Access-Control-Allow-Origin": "*"
  }
})

export default fetcher;